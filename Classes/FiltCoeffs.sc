// Main math source:
// Zölzer, Udo. DAFX : Digital Audio Effects. 2nd ed. Chichester, West Sussex, England ; Hoboken, NJ: Wiley, 2011.

FiltCoeffs {
	var a0, a1, a2, b1, b2, tanFac, cFac;
	var v0, h0, k, ksq, denom, v02sqrt;
	var kFac, b0, bM;
	var gFac;

	*new {
		^super.new
	}

	fosCoeffs { arg filterType ... args;
		^this.perform(('fosCoeffs' ++ filterType).asSymbol, *args)
	}

	// FOS AP
	fosCoeffsAP { arg freq, sampleRate;

		tanFac = tan(pi * freq / sampleRate);
		cFac = (tanFac - 1) / (tanFac + 1);

		a0 = cFac;
		a1 = 1;
		b1 = -1 * cFac;

		^[a0, a1, b1];

	}

	fosCoeffsLP { arg freq, sampleRate;

		tanFac = tan(pi * freq / sampleRate);
		cFac = (tanFac - 1) / (tanFac + 1);

		a0 = tanFac/(tanFac + 1);
		a1 = tanFac/(tanFac + 1);
		b1 = -1 * cFac;

		^[a0, a1, b1];

	}

	fosCoeffsHP { arg freq, sampleRate;
		var tanFac, cFac, a0, a1, b1;

		tanFac = tan(pi * freq / sampleRate);
		cFac = (tanFac - 1) / (tanFac + 1);

		a0 = 1/(tanFac + 1);
		a1 = -1/(tanFac + 1);
		b1 = -1 * cFac;

		^[a0, a1, b1];

	}

	fosCoeffsLS { arg freq, g, sampleRate;
		var tanFac, cFac, a0, a1, b1, v0, h0;

		v0 = 10**(g/20);

		h0 = v0 - 1;

		tanFac = tan(pi * freq / sampleRate);
		cFac = (g > 0).if({
			(tanFac - 1) / (tanFac + 1)
		}, {
			(tanFac - v0)/ (tanFac + v0)
		});

		// filter coefficients
		a0 = cFac;
		a1 = h0/2;
		b1 = -1 * cFac;

		// return
		^[a0, a1, b1];
	}

	fosCoeffsHS { arg freq, g, sampleRate;

		v0 = 10**(g/20);

		h0 = v0 - 1;

		tanFac = tan(pi * freq / sampleRate);
		cFac = (g > 0).if({
			(tanFac - 1) / (tanFac + 1)
		}, {
			(v0 * tanFac - 1)/ (v0 * tanFac + 1)
		});

		a0 = cFac;
		a1 = h0/2;
		b1 = -1 * cFac;

		^[a0, a1, b1];
	}

	sosCoeffs { arg filterType ... args;
		^this.perform(('sosCoeffs' ++ filterType).asSymbol, *args)
	}

	sosCoeffsAP { arg freq, q, sampleRate;

		k = tan(pi * freq / sampleRate);
		ksq = k.squared;
		denom = (ksq * q) + k + q;

		a0 = ((ksq * q) - k + q)/denom;
		a1 = 2 * q * (ksq - 1)/denom;
		a2 = 1;
		b1 = -2 * (q * (ksq - 1))/denom;
		b2 = -1 * ((ksq * q) - k + q)/denom;

		^[a0, a1, a2, b1, b2];

	}

	sosCoeffsLP { arg freq, q, sampleRate;

		k = tan(pi * freq / sampleRate);
		ksq = k.squared;
		denom = (ksq * q) + k + q;

		a0 = ksq * q/denom;
		a1 = 2 * ksq * q/denom;
		a2 = a0;
		b1 = -2 * (q * (ksq - 1))/denom;
		b2 = -1 * ((ksq * q) - k + q)/denom;

		^[a0, a1, a2, b1, b2];

	}

	sosCoeffsHP { arg freq, q, sampleRate;

		k = tan(pi * freq / sampleRate);
		ksq = k.squared;
		denom = (ksq * q) + k + q;

		a0 = q/denom;
		a1 = -2 * q/denom;
		a2 = a0;
		b1 = -2 * (q * (ksq - 1))/denom;
		b2 = -1 * ((ksq * q) - k + q)/denom;

		^[a0, a1, a2, b1, b2];

	}

	sosCoeffsBP { arg freq, q, sampleRate;

		k = tan(pi * freq / sampleRate);
		ksq = k.squared;
		denom = (ksq * q) + k + q;

		a0 = k/denom;
		a1 = 0;
		a2 = -1 * a0;
		b1 = -2 * (q * (ksq - 1))/denom;
		b2 = -1 * ((ksq * q) - k + q)/denom;

		^[a0, a1, a2, b1, b2];

	}

	sosCoeffsBR { arg freq, q, sampleRate;

		k = tan(pi * freq / sampleRate);
		ksq = k.squared;
		denom = (ksq * q) + k + q;

		a0 = (q * (1 + ksq))/denom;
		a1 = 2 * (q * (ksq - 1))/denom;
		a2 = a0;
		b1 = -2 * (q * (ksq - 1))/denom;
		b2 = -1 * ((ksq * q) - k + q)/denom;

		^[a0, a1, a2, b1, b2];

	}

	sosCoeffsLS { arg freq, g, sampleRate;

		k = tan(pi * freq / sampleRate);
		ksq = k.squared;
		v0 = 10**(g/20);
		v02sqrt = (2 * v0).sqrt;

		(g > 0).if({
			denom = 1 + (2.sqrt * k) + ksq;
			a0 = (1 + (v02sqrt * k) + (v0 * ksq))/denom;
			a1 = (2 * (v0 * ksq - 1))/denom;
			a2 = (1 - (v02sqrt * k) + (v0 * ksq))/denom;
			b1 = -1 * (2 * (ksq - 1))/denom;
			b2 = -1 * (1 - (2.sqrt * k) + ksq)/denom;
		}, {
			denom = v0 + (v02sqrt * k) + ksq;
			a0 = (v0 * (1 + (2.sqrt * k) + ksq))/denom;
			a1 = (2 * v0 * (ksq - 1))/denom;
			a2 = (v0 * (1 - (2.sqrt * k) + ksq))/denom;
			b1 = -1 * (2 * (ksq - v0))/denom;
			b2 = -1 * (v0 - (v02sqrt * k) + ksq)/denom;
		});

		^[a0, a1, a2, b1, b2];
	}

	sosCoeffsHS { arg freq, g, sampleRate;

		k = tan(pi * freq / sampleRate);
		ksq = k.squared;
		v0 = 10**(g/20);
		v02sqrt = (2 * v0).sqrt;

		(g > 0).if({
			denom = 1 + (2.sqrt * k) + ksq;
			a0 = (v0 + (v02sqrt * k) + ksq)/denom;
			a1 = (2 * (ksq - v0))/denom;
			a2 = (v0 - (v02sqrt * k) + ksq)/denom;
			b1 = -1 * (2 * (ksq - 1))/denom;
			b2 = -1 * (1 - (2.sqrt * k) + ksq)/denom;
		}, {
			denom = 1 + (v02sqrt * k) + (v0 * ksq);
			a0 = (v0 * (1 + (2.sqrt * k) + ksq))/denom;
			a1 = (2 * v0 * (ksq - 1))/denom;
			a2 = (v0 * (1 - (2.sqrt * k) + ksq))/denom;
			b1 = -1 * (2 * (v0 * ksq - 1))/denom;
			b2 = -1 * (1 - (v02sqrt * k) + (v0 * ksq))/denom;
		});

		^[a0, a1, a2, b1, b2];
	}

	sosCoeffsPK { arg freq, q, g, sampleRate;

		k = tan(pi * freq / sampleRate);
		ksq = k.squared;
		v0 = 10**(g/20);

		(g > 0).if({
			denom = 1 + (q.reciprocal * k) + ksq;
			a0 = (1 + (v0 / q * k) + ksq)/denom;
			a1 = (2 * (ksq - 1))/denom;
			a2 = (1 - (v0 / q * k) + ksq)/denom;
			b1 = -1 * (2 * (ksq - 1))/denom;
			b2 = -1 * (1 - (q.reciprocal * k) + ksq)/denom;
		}, {
			denom = 1 + (q.reciprocal * v0.reciprocal * k) + ksq;
			a0 = (1 + (q.reciprocal * k) + ksq)/denom;
			a1 = (2 * (ksq - 1))/denom;
			a2 = (1 - (q.reciprocal * k) + ksq)/denom;
			b1 = -1 * (2 * (ksq - 1))/denom;
			b2 = -1 * (1 - (k / (q * v0)) + ksq)/denom;
		});

		// return
		^[a0, a1, a2, b1, b2];
	}

	// Low-pass filter coefficients - SOS
	sosLPCoeffs  { arg freq, dampFac, sampleRate;

		cFac = 1/tan(pi * freq / sampleRate);

		a0 = 1/(1 + (2 * dampFac * cFac) + (cFac.squared));
		a1 = 2 * a0;
		a2 = a0;
		b1 = -1 * 2 * a0 * (1 - (cFac.squared));
		b2 = -1 * a0 * (1 - (2 * dampFac * cFac) + (cFac.squared));

		^[a0, a1, a2, b1, b2];
	}

	dampFac { arg n, k;
		^sin((((2*(k + 1)) - 1) * pi) / (2 * n));
	}

	cascadeCoeffsLPDamp { arg freq, n, sampleRate;

		^((n/2).asInt).collect({ arg k;
			this.sosLPCoeffs(
				freq,
				this.dampFac(n, k),
				sampleRate
			)
		})
	}

	firCombCoeffs { arg gain;

		kFac = gain.dbamp;

		b0 = (1+kFac)/2;
		bM = (1-kFac)/2;

		^[b0, bM];
	}

	iirCombCoeffs { arg gain;

		kFac = gain.dbamp;

		gFac = (1-kFac)/(1+kFac);
		cFac = 1-gFac.abs;

		^[cFac, gFac];
	}

	schroederCoeffs { arg bFac;

		tanFac = tan(0.5pi * bFac);
		gFac = (1 - tanFac) / (1 + tanFac);

		a0 = -1 * gFac;
		a1 = 1;
		b1 = gFac;

		^[a0, a1, b1]
	}

	regaliaMitraCoeff { arg gain;
		^(gain.dbamp - 1) / 2;
	}
}
