+ AtsFile {

	asSignal {arg type = 'Freq', partialNumber;
		^this.perform(('getPar' ++ type).asSymbol, partialNumber).as(Signal)
	}

	asEnv {arg type = 'Freq', partialNumber, curve = 'sin';
		var data, levels, times;
		levels = this.perform(('getPar' ++ type).asSymbol, partialNumber);
		times = (levels.size - 1).collect{
			sndDur/numFrames
		};
		^Env.new(levels, times,  curve)
	}

	allEnvs {arg type = 'Freq', curve = 'sin';
		var data, envArray = [], levels, times;
		times = (numFrames.asInt - 1).collect{
			sndDur/numFrames
		};
		numPartials.do{arg partialNumber;
			levels = this.perform(('getPar' ++ type).asSymbol, partialNumber + 1);
			envArray = envArray.add(Env.new(levels, times,  curve))
		};

		^envArray
	}

}