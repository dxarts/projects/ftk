// filter toolkit

FTK {
	var <filtCoeffs;
	var <>ampEnvs, <>freqEnvs;

	*new {
		^super.new.init
	}

	init {
		filtCoeffs = FiltCoeffs.new;
	}

	fos { arg signal, filterType, freq, sampleRate;
		var output, coeffs, y0, y1;

		output = Signal.zeroFill(signal.size);

		y1 = 0.0;

		(signal.size).do{arg i;
			var res;
			coeffs = filtCoeffs.fosCoeffs(filterType, freq[i], sampleRate);
			res = this.fosFunc(signal[i], y1, coeffs);
			output[i] = res[0];
			y1 = res[1];

		};
		^output
	}

	fosFunc { |x1, y1, coeffs|
		var y0;
		y0 = x1 + (coeffs[2] * y1);
		^[(coeffs[0] * y0) + (coeffs[1] * y1), y0]
	}

	fosUGEN { arg signal, filterType, freq, sampleRate;
		var output, coeffs, multiplier, signalBuffer, dur, synth, cond = Condition.new;
		var score;

		score = CtkScore.new;
		signal.write("/tmp/fosInput.wav", sampleRate: sampleRate);
		signalBuffer = CtkBuffer("/tmp/fosInput.wav").addTo(score);
		dur = signalBuffer.duration;

		synth = CtkSynthDef('tempFOS', {
			arg inBuf;
			var sig, response;
			freq = freq.isKindOf(Env).if({EnvGen.kr(freq)}, {freq});
			coeffs = filtCoeffs.fosCoeffs(filterType, freq, sampleRate);

			sig = PlayBuf.ar(1, inBuf);

			response = FOS.ar(sig, coeffs[0], coeffs[1], coeffs[2]);

			Out.ar(0, response);
		});

		score.add(
			synth.note(0.0, dur)
			.inBuf_(signalBuffer)
		);

		score.write(
			path: "/tmp/fos.wav",
			headerFormat: 'WAV',
			sampleFormat: 'float',
			sampleRate: sampleRate,
			options: ServerOptions.new.numOutputBusChannels_(1),
			action: {cond.test_(true).signal}
		);
		cond.wait;

		output = Signal.read("/tmp/fos.wav", signal.size);
		^output



	}

	fosShelf { arg signal, filterType, freq, g, sampleRate;
		var output, coeffs, y1, y2, y0, yl, sign;

		sign = (filterType == 'LS').if({1}, {-1});

		output = Signal.zeroFill(signal.size);

		y1 = 0.0;
		y2 = 0.0;

		signal.size.do{arg i;
			coeffs = filtCoeffs.perform(('fosCoeffs' ++ filterType).asSymbol, freq[i], g[i], sampleRate);
			y0 = signal[i] + (coeffs[2] * y1);
			yl = (coeffs[0] * y0) + y1;
			output[i] = coeffs[1] * (signal[i] + (sign * yl)) + signal[i];
			y2 = y1;
			y1 = y0


		};

		^output
	}


	sos { arg signal, filterType, freq, q, sampleRate;
		var output, coeffs, y1, y2, y0;

		output = Signal.zeroFill(signal.size);

		y1 = 0.0;
		y2 = 0.0;

		signal.size.do{arg i;
			var res;
			coeffs = filtCoeffs.sosCoeffs(filterType, freq[i], q[i], sampleRate);
			res = this.sosFunc(signal[i], y1, y2, coeffs);
			output[i] = res[0];
			y2 = res[1];
			y1 = res[2];
		};

		^output
	}

	sosFunc { |x1, y1, y2, coeffs|
		var y0;
		y0 = x1 + (coeffs[3] * y1) + (coeffs[4] * y2);
		^[(coeffs[0] * y0) + (coeffs[1] * y1) + (coeffs[2] * y2), y1, y0]
	}

	sosShelf { arg signal, filterType, freq, g, sampleRate;
		var output, coeffs, y1, y2, y0;

		output = Signal.zeroFill(signal.size);

		y1 = 0.0;
		y2 = 0.0;

		signal.size.do{arg i;
			coeffs = filtCoeffs.perform(('sosCoeffs' ++ filterType).asSymbol, freq[i], g[i], sampleRate);
			y0 = signal[i] + (coeffs[3] * y1) + (coeffs[4] * y2);
			output[i] = (coeffs[0] * y0) + (coeffs[1] * y1) + (coeffs[2] * y2);
			y2 = y1;
			y1 = y0


		};

		^output
	}

	sosPeak { arg signal, freq, q, g, sampleRate;
		var output, coeffs, y1, y2, y0;

		output = Signal.zeroFill(signal.size);

		y1 = 0.0;
		y2 = 0.0;

		signal.size.do{arg i;
			coeffs = filtCoeffs.sosCoeffsPK(freq[i], q[i], g[i], sampleRate);
			y0 = signal[i] + (coeffs[3] * y1) + (coeffs[4] * y2);
			output[i] = (coeffs[0] * y0) + (coeffs[1] * y1) + (coeffs[2] * y2);
			y2 = y1;
			y1 = y0


		};

		^output
	}

	sosUGEN { arg signal, filterType, freq, q, sampleRate;
		var output, coeffs, multiplier, signalBuffer, outputBuffer, size, dur, synth, cond = Condition.new;
		var score;


		score = CtkScore.new;
		signal.write("/tmp/sosInput.wav", sampleRate: sampleRate);
		signalBuffer = CtkBuffer("/tmp/sosInput.wav").addTo(score);
		dur = signalBuffer.duration;
		size = signalBuffer.numFrames;
		outputBuffer = CtkBuffer.buffer(size).addTo(score);

		synth = CtkSynthDef('tempSOS', {
			arg inBuf;
			var sig, response;
			freq = freq.isKindOf(Env).if({EnvGen.kr(freq)}, {freq});
			q = q.isKindOf(Env).if({EnvGen.kr(q)}, {q});
			coeffs = filtCoeffs.sosCoeffs(filterType, freq, q, sampleRate);

			sig = PlayBuf.ar(1, inBuf);

			response = SOS.ar(sig, coeffs[0], coeffs[1], coeffs[2], coeffs[3], coeffs[4]);

			Out.ar(0, response);
		});

		score.add(
			synth.note(0.0, dur)
			.inBuf_(signalBuffer)
		);

		score.write(
			path: "/tmp/sos.wav",
			headerFormat: 'WAV',
			sampleFormat: 'float',
			sampleRate: sampleRate,
			options: ServerOptions.new.numOutputBusChannels_(1),
			action: {cond.test_(true).signal}
		);
		cond.wait;

		output = Signal.read("/tmp/sos.wav", signal.size);

		^output

	}

	sosUGENShelf { arg signal, filterType, freq, g, sampleRate;
		var output, coeffs, multiplier, signalBuffer, outputBuffer, size, dur, synth, cond = Condition.new;
		var score;


		score = CtkScore.new;
		signal.write("/tmp/sosInput.wav", sampleRate: sampleRate);
		signalBuffer = CtkBuffer("/tmp/sosInput.wav").addTo(score);
		dur = signalBuffer.duration;
		size = signalBuffer.numFrames;
		outputBuffer = CtkBuffer.buffer(size).addTo(score);

		synth = CtkSynthDef('tempSOS', {
			arg inBuf;
			var sig, response;
			freq = freq.isKindOf(Env).if({EnvGen.kr(freq)}, {freq});
			g = g.isKindOf(Env).if({EnvGen.kr(g)}, {g});
			coeffs = filtCoeffs.sosCoeffs(filterType, freq, g, sampleRate);

			sig = PlayBuf.ar(1, inBuf);

			response = SOS.ar(sig, coeffs[0], coeffs[1], coeffs[2], coeffs[3], coeffs[4]);

			Out.ar(0, response);
		});

		score.add(
			synth.note(0.0, dur)
			.inBuf_(signalBuffer)
		);

		score.write(
			path: "/tmp/sos.wav",
			headerFormat: 'WAV',
			sampleFormat: 'float',
			sampleRate: sampleRate,
			options: ServerOptions.new.numOutputBusChannels_(1),
			action: {cond.test_(true).signal}
		);
		cond.wait;

		output = Signal.read("/tmp/sos.wav", signal.size);

		^output

	}

	sosUGENPeak { arg signal, freq, q, g, sampleRate;
		var output, coeffs, multiplier, signalBuffer, dur, synth, cond = Condition.new;
		var score;


		score = CtkScore.new;
		signal.write("/tmp/sosInput.wav", sampleRate: sampleRate);
		signalBuffer = CtkBuffer("/tmp/sosInput.wav").addTo(score);
		dur = signalBuffer.duration;

		synth = CtkSynthDef('tempSOS', {
			arg inBuf;
			var sig, response;
			freq = freq.isKindOf(Env).if({EnvGen.kr(freq)}, {freq});
			q = q.isKindOf(Env).if({EnvGen.kr(q)}, {q});
			g = g.isKindOf(Env).if({EnvGen.kr(g)}, {g});
			coeffs = filtCoeffs.sosCoeffsPK(freq, q, g, sampleRate);

			sig = PlayBuf.ar(1, inBuf);

			response = SOS.ar(sig, coeffs[0], coeffs[1], coeffs[2], coeffs[3], coeffs[4]);

			Out.ar(0, response);
		});

		score.add(
			synth.note(0.0, dur)
			.inBuf_(signalBuffer)
		);

		score.write(
			path: "/tmp/sos.wav",
			headerFormat: 'WAV',
			sampleFormat: 'float',
			sampleRate: sampleRate,
			options: ServerOptions.new.numOutputBusChannels_(1),
			action: {cond.test_(true).signal}
		);
		cond.wait;

		output = Signal.read("/tmp/sos.wav", signal.size);

		^output

	}


	sosCascade { arg signal, n, freq, q, sampleRate;
		var output, coeffs, y1, y2, y0;

		output = signal.copy;

		(n/2).asInteger.do{ arg k;

			y1 = 0.0;
			y2 = 0.0;

			signal.size.do{arg i;
				coeffs = filtCoeffs.sosCoeffs(freq[i], q[i], sampleRate);
				y0 = output[i] + (coeffs[3] * y1) + (coeffs[4] * y2);
				output[i] = (coeffs[0] * y0) + (coeffs[1] * y1) + (coeffs[2] * y2);
				y2 = y1;
				y1 = y0

			}
		};

		^output
	}

	sosCascadeLP { arg signal, freq, n, sampleRate;
		var output, coeffs, y1, y2, y0;

		output = signal.copy;

		(n/2).asInteger.do{ arg k;

			y1 = 0.0;
			y2 = 0.0;

			signal.size.do{arg i;
				coeffs = filtCoeffs.cascadeCoeffsLPDamp(freq[i], n, sampleRate)[k];
				y0 = output[i] + (coeffs[3] * y1) + (coeffs[4] * y2);
				output[i] = (coeffs[0] * y0) + (coeffs[1] * y1) + (coeffs[2] * y2);
				y2 = y1;
				y1 = y0


			}
		};

		^output
	}

	// digital state variable filter
	dsvf { arg signal, filterType, freq, q, sampleRate;
		var yl, yb, yh, f1, q1;
		var outputs;

		outputs = IdentityDictionary.new(know: true);
		outputs.putPairs([
			'HP', Signal.newClear(signal.size),
			'LP', Signal.newClear(signal.size),
			'BP', Signal.newClear(signal.size)
		]);

		q1 = q.reciprocal;
		f1 = 2 * sin(pi * freq / sampleRate);

		yl = 0.0;
		yb = 0.0;

		signal.size.do{arg i;

			yh = signal[i] - yl - (q1 * yb);
			yb = (f1 * yh) + yb;
			yl = (f1 * yb) + yl;

			outputs['HP'][i] = yh;
			outputs['LP'][i] = yl;
			outputs['BP'][i] = yb;


		};

		^outputs[filterType]
	}

	firComb { arg signal, freq, gain, evenOdd = 'even', sampleRate;
		var delaySamples, memDelay, prevMemDelay, output, b0, bM;

		output = signal.copy;
		prevMemDelay = (freq[0].combDelay * sampleRate).asInteger.collect{0.0};


		signal.do{arg sample, i;
			#b0, bM = filtCoeffs.firCombCoeffs(gain[i]);
			bM = bM * (evenOdd == 'even').if({1}, {-1});
			delaySamples = (freq.combDelay * sampleRate).asInteger;
			memDelay = delaySamples.collect({0.0});
			memDelay = (prevMemDelay.size >= memDelay.size).if({
				prevMemDelay[0..delaySamples-1]
			}, {
				prevMemDelay.size.collect{arg i;
					prevMemDelay[i]
				} ++ memDelay[prevMemDelay.size..delaySamples-1]
			});
			output[i] = (signal[i] * b0) + (bM * memDelay.last);
			prevMemDelay = memDelay.rotate(1);
			prevMemDelay[0] = signal[i];
		};
		^output
	}

	iirComb { arg signal, freq, gain, evenOdd = 'even', sampleRate;
		var delaySamples, memDelay, prevMemDelay, output, cFac, gFac, sign;

		output = signal.copy;
		prevMemDelay = (freq[0].combDelay * sampleRate).asInteger.collect{0.0};
		sign = (evenOdd == 'even').if({1}, {-1});
		signal.do{arg sample, i;
			#cFac, gFac = filtCoeffs.iirCombCoeffs(gain[i]);
			delaySamples = (freq[i].combDelay * sampleRate).asInteger;
			memDelay = delaySamples.collect({0.0});
			memDelay = (prevMemDelay.size >= memDelay.size).if({
				prevMemDelay[0..delaySamples-1]
			}, {
				prevMemDelay.size.collect{arg i;
					prevMemDelay[i]
				} ++ memDelay[prevMemDelay.size..delaySamples-1]
			});
			output[i] = (signal[i] * cFac) + (sign * gFac * memDelay.last);
			prevMemDelay = memDelay.rotate(1);
			prevMemDelay[0] = output[i];
		};
		^output
	}

	schroederAP { arg signal, freq, bFac, sampleRate;
		var output, coeffs, y0, delaySamples, memDelay, prevMemDelay;

		output = Signal.zeroFill(signal.size);

		prevMemDelay = (freq[0].combDelay * sampleRate).asInteger.collect{0.0};

		(signal.size).do{arg i;
			delaySamples = (freq[i].combDelay * sampleRate).asInteger;
			memDelay = delaySamples.collect({0.0});
			memDelay = (prevMemDelay.size >= memDelay.size).if({
				prevMemDelay[0..delaySamples-1]
			}, {
				prevMemDelay.size.collect{arg i;
					prevMemDelay[i]
				} ++ memDelay[prevMemDelay.size..delaySamples-1]
			});
			coeffs = filtCoeffs.schroederCoeffs(bFac[i]);
			y0 = signal[i] + (coeffs[2] * memDelay.last);
			output[i] = (coeffs[0] * y0) + (coeffs[1] * memDelay.last);
			prevMemDelay = memDelay.rotate(1);
			prevMemDelay[0] = y0;
		};
		^output
	}

	universalComb { arg signal, freq, gain, bFac, evenOdd = 'even', sampleRate;
		^signal + (filtCoeffs.regaliaMitraCoeff(gain) * (this.schroederAP(signal, freq, bFac, sampleRate) * (evenOdd == 'even').if({-1}, {1}) + signal))
	}


	// create lowpass filter
	lowpassKernel { arg size, freq, sampleRate;
		var bandwidth = 2 * freq / sampleRate;
		^size.collect({arg n;
			bandwidth * (bandwidth * (n - ((size-1)/2))).sincPi
		}).as(Signal)
	}

	highpassKernel { arg size, freq, sampleRate;
		^this.lowpassKernel(size, sampleRate/2, sampleRate) - this.lowpassKernel(size, freq, sampleRate)
	}

	bandpassKernel { arg size, freq, q, sampleRate;
		var bw = freq/q;
		^this.lowpassKernel(size, freq + (bw/2), sampleRate) - this.lowpassKernel(size, freq - (bw/2), sampleRate)

	}

	// sincPi bandstop (complementary) - undefined!
	bandstopKernel { arg size, freq, q, sampleRate;
		var bw = freq/q;
		^size.collect({arg n;
			(n - ((size-1)/2)).sincPi
		}).as(Signal) - this.lowpassKernel(size, freq + (bw/2), sampleRate) + this.lowpassKernel(size, freq - (bw/2), sampleRate)
	}

	lowshelfKernel { arg size, freq, gain, sampleRate;
		^this.highpassKernel(size, freq, sampleRate) + (this.lowpassKernel(size, freq, sampleRate) * gain.dbamp)
	}

	highshelfKernel { arg size, freq, gain, sampleRate;
		^(this.highpassKernel(size, freq, sampleRate) * gain.dbamp) + this.lowpassKernel(size, freq, sampleRate)
	}

	testFilter { arg filterType, size ... args;
		var plotter, plotDbMin, plotDbMax, probe, signal;
		var fftResponse, fftMagnitude, bounds;
		{
			{bounds = Window.screenBounds;}.defer;
			// create probe signal, an impulse
			probe = Signal.newClear(size);
			probe.put((size/2).asInteger, 1.0);

			filterType.asString.contains("Kernel").if({
				signal = this.perform(filterType, size, *args);
				signal = signal.zeroPad(size.nextPowerOfTwo);
				size = signal.size
			}, {
				signal = this.perform(filterType, probe, *args);
			});
			// FFT analysis here!
			fftResponse = fft(
				signal,
				Signal.newClear(size),
				Signal.fftCosTable(size)
			);

			// find (& trim magnitude)
			fftMagnitude = fftResponse.magnitude;
			fftMagnitude = fftMagnitude.copyFromStart((size/2).asInteger);

			// plot paramaters
			{plotDbMin = -80.0;
				plotDbMax = fftMagnitude.maxItem.ampdb + 6;
				plotter = Plotter("Frequency Response", bounds);
				plotter.value_(fftMagnitude.ampdb);
				plotter.domainSpecs = ControlSpec(0, args.last/2, units: "Hz");
				plotter.setProperties(\labelX, "Frequency");
				plotter.minval_(plotDbMin).maxval_(plotDbMax);
				plotter.refresh;}.defer
		}.forkIfNeeded


	}

	fftEnv { arg signal, fftSize = 2048, hopSize = 0.25, sampleRate = 48000;
		var hopSizeSeconds, fftEnvArray;
		var times, levels, index;
		var mag, fft, clipSig;
		var magNorm = (Array.with(1/fftSize) ++ Array.fill(fftSize/2 - 1, { 2/fftSize }));  // fft mag normalisation
		hopSizeSeconds = hopSize * fftSize / sampleRate;

		fftEnvArray = [];

		index = 0;

		levels = (fftSize/2).asInteger.collect{[]};

		while({index < (signal.size - fftSize)}, {
			clipSig = signal[index..index + (fftSize - 1)];
			clipSig = clipSig.zeroPad(fftSize) * Signal.hanningWindow(fftSize);
			fft = clipSig.fft(Signal.newClear(fftSize), Signal.fftCosTable(fftSize));
			mag = fft.magnitude.keep((fftSize/2).asInteger) * magNorm;
			(fftSize/2).asInteger.do{arg i;
				levels[i] = levels[i].add(mag[i])
			};
			index = (index + (fftSize * hopSize)).asInteger;
		});

		times = (levels[0].size - 1).collect{arg i;
			hopSizeSeconds
		};

		levels.do{arg level;
			fftEnvArray = fftEnvArray.add(Env(level, times, 'sin'))
		};

		^fftEnvArray
	}


	vocodeServer { arg signal, q = 10, sampleRate = 48000, blockSize = 64;
		var score, buffer, synth, ampPaths, freqPaths, freqBuffers, ampBuffers, controlRate, outPath, cond = Condition.new;
		var numBuffers, bufferPath;
		outPath = "/tmp/vocodeOutput.wav";
		controlRate = (sampleRate / blockSize).asInteger;
		bufferPath = "/tmp/vocode.wav";
		signal.write(bufferPath, sampleRate: sampleRate);
		ampPaths = ampEnvs.collect{arg env, i;
			var path = "/tmp/amp_" ++ i ++ ".wav";
			env.asSignal((env.totalDuration * controlRate).asInteger).write(path, sampleRate: controlRate);
			path
		};

		freqPaths = freqEnvs.collect{arg env, i;
			var path = "/tmp/freq_" ++ i ++ ".wav";
			env.asSignal((env.totalDuration * controlRate).asInteger).write(path, sampleRate: controlRate);
			path
		};
		q = q.isKindOf(Env).if({CtkControl.env(q)}, {q});
		numBuffers = ampPaths.size + freqPaths.size + 1;
		// Server.default.quit;
		Server.default.options.maxNodes_(2**14).numBuffers_(numBuffers);
		Server.default.newAllocators;
		score = CtkScore.new;
		buffer = CtkBuffer.playbuf(bufferPath).addTo(score);
		ampBuffers = ampPaths.collect{arg path;
			CtkBuffer(path).addTo(score)
		};
		freqBuffers = freqPaths.collect{arg path;
			CtkBuffer(path).addTo(score)
		};
		synth = CtkSynthDef.new('atsVocode', {
			arg buffer, ampBuf, freqBuf, q;
			var sig, amp, freq;
			amp = PlayBuf.ar(1, ampBuf, BufRateScale.kr(ampBuf));
			freq = PlayBuf.ar(1, freqBuf, BufRateScale.kr(freqBuf));
			sig = PlayBuf.ar(buffer.numChannels, buffer, BufRateScale.kr(buffer), loop: 1);
			Out.ar(0, BPF.ar(sig, freq, q.reciprocal) * amp);
		});
		ampBuffers.do{arg ampBuffer, i;
			var freqBuffer = freqBuffers[i];

			score.add(
				synth.note(duration: ampBuffer.duration)
				.buffer_(buffer)
				.ampBuf_(ampBuffer)
				.freqBuf_(freqBuffer)
				.q_(q)
			)
		};

		score.write(
			path: outPath,
			headerFormat: 'WAV',
			sampleFormat: 'float',
			sampleRate: sampleRate,
			options: ServerOptions.new.numOutputBusChannels_(buffer.numChannels).maxNodes_(2**14).memSize_(2**21).numBuffers_(numBuffers),
			saveFilePath: Platform.userHomeDir ++ "/Desktop/vocodeScore.sc",
			action: {cond.test_(true).signal}
		);

		cond.wait;

		^Signal.read(outPath);


	}

	generateEnvs { arg signal, fftSize = 2048, hopSize = 0.25, sampleRate = 48000;
		ampEnvs = this.fftEnv(signal, fftSize, hopSize, sampleRate);
		freqEnvs = ampEnvs.collect{arg ampEnv, i;
			Env((sampleRate/fftSize * i).dup, [ampEnv.totalDuration])
		};
	}

	readAtsEnvs { arg atsFile;
		ampEnvs = atsFile.allEnvs('Amp');
		freqEnvs = atsFile.allEnvs('Freq');
	}

	vocode { arg signal, q = 10, sampleRate = 48000;
		var vocodeSignal;
		vocodeSignal = Signal.newClear(signal.size);
		ampEnvs.do{arg ampEnv, i;
			var freqEnv = freqEnvs[i];
			vocodeSignal = vocodeSignal + (this.sos(signal, 'BP', freqEnv.asSignal(signal.size), q, sampleRate) * ampEnv.asSignal(signal.size))
		};

		^vocodeSignal
	}

	vocodeUGEN { arg signal, q = 10, sampleRate = 48000;
		var vocodeSignal;
		vocodeSignal = Signal.newClear(signal.size);
		ampEnvs.do{arg ampEnv, i;
			var freqEnv = freqEnvs[i];
			vocodeSignal = vocodeSignal + (this.sosUGEN(signal, 'BP', freqEnv, q, sampleRate) * ampEnv.asSignal(signal.size));
		};

		^vocodeSignal
	}

	preConditionSignal { arg signal, reverse = true;
		var mean, peak, size;
		// calculate how much to zero pad
		size = signal.size.nextPowerOfTwo - signal.size;
		// calculate bias
		mean = signal.mean;
		// remove bias
		signal.discardDC;
		// find peak
		peak = signal.peak;
		// scale to -1 to 1
		signal.scale(peak.reciprocal);
		// zero pad to next power of two
		signal = signal.zeroPad(signal.size.nextPowerOfTwo);
		reverse.if({
			// reverse and concatenate to original
			signal = signal.copy.reverse ++ signal;
		});
		^[signal, mean, peak, size]
	}

	postConditionSignal { arg signal, mean, peak, size, reverse = true;
		reverse.if({
			// chop of reversed doubling of signal
			signal = signal[(signal.size/2).asInteger..signal.size - size - 1];
		});
		// scale back to peak
		signal.scale(peak);
		// reintroduce bias
		signal.offset(mean);
		^signal
	}

}