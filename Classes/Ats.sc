Ats {
	var <atsFilePath, <>soundFilePath;
	var flags;
	classvar <>atsCommandPath, <defaultFlags;

	*initClass {
		atsCommandPath = File.realpath(this.filenameSymbol).asPathName.parentPath;
		atsCommandPath = File.realpath(atsCommandPath ++ "/../ATS/atsa");
		defaultFlags = "-b0.0 -e0.0 -l20.0 -H20000.0 -d0.1 -h0.25 -m-60.0";
	}

	*new { arg atsFilePath;
		^super.newCopyArgs(atsFilePath)
	}

	loadSignal { arg signal, sampleRate = 48000;
		soundFilePath = atsFilePath.replaceExtension("wav");
		signal.write(soundFilePath, sampleRate: sampleRate);
	}

	setFlags {arg flagList;
		flags = flagList
	}

	availableFlags {
		"Flags:
	 -b start (0.000000 seconds)
	 -e duration (0.000000 seconds or end)
	 -l lowest frequency (20.000000 Hertz)
	 -H highest frequency (20000.000000 Hertz)
	 -d frequency deviation (0.100000 of partial freq.)
	 -c window cycles (4 cycles)
	 -w window type (type: 1)
		(Options: 0=BLACKMAN, 1=BLACKMAN_H, 2=HAMMING, 3=VONHANN)
	 -h hop size (0.250000 of window size)
	 -m lowest magnitude (-60.000000)
	 -t track length (3 frames)
	 -s min. segment length (3 frames)
	 -g min. gap length (3 frames)
	 -T SMR threshold (30.000000 dB SPL)
	 -S min. segment SMR (60.000000 dB SPL)
	 -P last peak contribution (0.000000 of last peak's parameters)
	 -M SMR contribution (0.500000)
	 -F File Type (type: 4)
(Options: 1=amp.and freq. only, 2=amp.,freq. and phase, 3=amp.,freq. and residual, 4=amp.,freq.,phase, and residual".postln
	}

	atsa {
		flags = flags ? defaultFlags;
		(atsCommandPath + soundFilePath + atsFilePath + flags).postln.systemCmd;
		"ATS analysis completed!".postln;
		^AtsFile.new(atsFilePath)
	}

}


